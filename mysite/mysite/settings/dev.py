from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-3xgln-(2o&eyz)5$mdr^nf_!-v%@xr1&o_7ic*f+8=8t_ng=)'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*'] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS = INSTALLED_APPS + [
	'debug_toolbar',
	'django_extensions',
	'wagtail.contrib.styleguide',
]

MIDDLEWARE = MIDDLEWARE + [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = [
    '127.0.0.1',
  	'172.17.0.1',
]

CACHES = {
	"default": {
		"BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
		"LOCATION": "/home/radimk/Projects/Wagtail/wagtailcms/mysite/cache"

	}
}

try:
    from .local import *
except ImportError:
    pass
