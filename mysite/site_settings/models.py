from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting

@register_setting
class SocialMediaSetting(BaseSetting):
	"""Social media settings for custom wesite."""

	facebook = models.URLField(blank=True, null=True, help_text="facebook URL")
	twitter = models.URLField(blank=True, null=True, help_text="Twitter URL")
	youtube = models.URLField(blank=True, null=True, help_text="Youtube Chanel URL")

	panels = [
		MultiFieldPanel([
			FieldPanel("facebook"),
			FieldPanel("twitter"),
			FieldPanel("youtube"),
		], heading="Social Media Settinds")
	]
